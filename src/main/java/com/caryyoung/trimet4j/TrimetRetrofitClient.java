package com.caryyoung.trimet4j;

import com.caryyoung.trimet4j.model.detours.DetoursResultSet;
import com.caryyoung.trimet4j.model.stoplocation.StopLocationResultSet;
import com.caryyoung.trimet4j.model.tripplanner.api.schema.org.trimet.maps.maps.model.xml.tripplanner.TripPlannerResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Retrofit Client for the Trimet API.
 * https://developer.trimet.org/ws_docs/
 */
public interface TrimetRetrofitClient {

    /**
     * Make a request to the Trimet Trip Planner API
     * See https://developer.trimet.org/ws_docs/tripplanner_ws.shtml for official documentation
     *
     * @param appId (REQUIRED) your application ID, supplied by TriMet.  To obtain an application ID, visit
     *              https://developer.trimet.org/appid/registration/
     * @param fromCoord The GPS coordinates of the origin of the trip.  This should be a {@link String}
     *                  representation of a decimal number between -122.72,45.51.
     * @param fromPlace The name of the origin of the trip (e.g. "zoo"). If a {@code fromCoord} is also supplied,
     *                  this will simply be used as a label. If not, TriMet will attempt to discern the location
     *                  of this place.
     * @param toPlace The name of the destination of the trip (e.g. "zoo"). If a {@code toCoord} is also
     *                supplied, this will simply be used as a label. If not, TriMet will attempt to discern the
     *                location of this place.
     * @param toCoord The GPS coordinates of the destination of the trip.  This should be a {@link String}
     *                representation of a decimal number between -122.72,45.51.
     * @param date String representation of the Date of the planned trip in the format 'MM-dd-yyyy'
     *             (e.g. "05-15-2017"). If no date is given, then current date (from a server in Portland, OR)
     *             is used.
     * @param time (REQUIRED) String representation of the time of the planned trip, in format 'hh:mm aa'
     *             (e.g. "10:22 am")
     * @param min String of one of three possible values: "T", "X", or "W". Itineraries that offer the
     *            Quickest Trip (T); Fewest Transfers (X); or Shortest Walk (W) -- T is the default. NOTE:
     *            the "Shortest Walk (W)" option may give circuitus routing just to avoid a few steps.
     * @param arr String of one of two possible values: "D" for "Depart by Time" or "A" "Arrive by time",
     *            which conveys the intended meaning of the {@code time} parameter.
     * @param walk decimal value between {@code 0.01} and {@code 0.999} that describes the maximum desired
     *             walking in miles for the trip.
     * @param mode Supply one of 3 values describing the mode of transportation: "A" for all modes, "B" for bus-only,
     *             or "T" for train-only.
     * @param format Currently, the only accepted value for this parameter is "XML". It's fine to omit it.
     * @param maxItineraries {@code String} representation of an integer between 1 and 6, which represents the
     *                       maximum number of itineraries to return. This defaults to 3.
     * @return a {@link TripPlannerResponse} describing itineraries that satisfy the supplied trip query.
     */
    @GET("ws/V1/trips/tripplanner")
    Call<TripPlannerResponse> tripPlanner(@Query("appId") String appId,
                                          @Query("fromCoord") String fromCoord,
                                          @Query("fromPlace") String fromPlace,
                                          @Query("toPlace") String toPlace,
                                          @Query("toCoord") String toCoord,
                                          @Query("Date") String date,
                                          @Query("Time") String time,
                                          @Query("Min") String min,
                                          @Query("Arr") String arr,
                                          @Query("Walk") Float walk,
                                          @Query("Mode") String mode,
                                          @Query("Format") String format,
                                          @Query("MaxItineraries") String maxItineraries);


    /**
     * Make a Request to the Trimet Stop Location API
     * See https://developer.trimet.org/ws_docs/stop_location_ws.shtml for official documentation
     *
     * @param appId (REQUIRED) your application ID, supplied by TriMet.  To obtain an application ID, visit
     *          https://developer.trimet.org/appid/registration/
     * @param bbox (optional) four comma-separated (no spaces) decimal values representing the lat/long of the
     *          lower-left and upper-right corners of a "boundary box" within which all Stop Locations will be returned.
     * @param ll (optional) comma-separated (no spaces) decimal lat/long pair representing the central point from which
     *          all Stop Locations within a specified search radius will be returned.
     * @param feet (optional) size of search radius in feet from the central point specified with 'll'.
     * @param meters (optional) size of search radius in meters from the central point specified with 'll'.
     * @return a {@link StopLocationResultSet} containing information about every Stop Location (including the routes
     *          served by each) within the specified boundaries or radius.
     */
    @GET("ws/V1/stops?json=true&showRouteDirs=true")
    Call<StopLocationResultSet> stopLocation(@Query("appId") String appId,
                                             @Query("bbox")String bbox,
                                             @Query("ll") String ll,
                                             @Query("feet") Integer feet,
                                             @Query("meters") Integer meters);



    @GET("ws/V1/detours?json=true&infolink=true")
    Call<DetoursResultSet> detours(@Query("appId") String appId,
                                   @Query("routes")String routes,
                                   @Query("includeFuture") boolean includeFuture);

}
