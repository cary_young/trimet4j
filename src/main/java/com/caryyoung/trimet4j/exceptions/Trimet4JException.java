package com.caryyoung.trimet4j.exceptions;

/**
 * Created by cary on 3/13/18.
 */
public class Trimet4JException extends RuntimeException {

    public Trimet4JException(String message, Throwable cause) {
        super(message, cause);
    }
    public Trimet4JException( Throwable cause) {
        super(cause);
    }
    public Trimet4JException(String message) {
        super(message);
    }
}
