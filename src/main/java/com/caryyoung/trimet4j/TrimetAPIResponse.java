package com.caryyoung.trimet4j;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by cary on 3/13/18.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TrimetAPIResponse<T> {

    private T body;
    private String message;
    private int status;
}
