package com.caryyoung.trimet4j;

import com.caryyoung.trimet4j.model.detours.DetoursRequest;
import com.caryyoung.trimet4j.model.detours.DetoursResponse;
import com.caryyoung.trimet4j.model.detours.DetoursResultSet;
import com.caryyoung.trimet4j.model.stoplocation.StopLocationResultSet;
import com.caryyoung.trimet4j.model.tripplanner.api.schema.org.trimet.maps.maps.model.xml.tripplanner.TripPlannerResponse;
import com.caryyoung.trimet4j.exceptions.Trimet4JException;
import com.caryyoung.trimet4j.model.tripplanner.TripPlannerRequest;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by cary on 6/1/17.
 */
public class Trimet4J {

    private TrimetRetrofitClient trimetClientXml;
    private TrimetRetrofitClient trimetClientJson;

    private String appId;

    public Trimet4J(String applicationId) {
        final Retrofit retrofitXml = new Retrofit.Builder()
                .baseUrl("https://developer.trimet.org")
                .addConverterFactory(SimpleXmlConverterFactory.createNonStrict())
                .build();
         this.trimetClientXml = retrofitXml.create(TrimetRetrofitClient.class);

        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://developer.trimet.org")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        this.trimetClientJson = retrofit.create(TrimetRetrofitClient.class);

        this.appId = applicationId;
    }


    /**
     * Make a request to the Trimet Trip Planner API
     * @param request Trip Planner Request
     * @return Trip Planner response wrapped inside a {@link TrimetAPIResponse}; the {@code TrimetAPIResponse} provides
     *         HTTP status, message, and body -- which will be a {@link TripPlannerResponse} when successful, or {@code null} if
     *         the request failed.
     * @throws Trimet4JException when there is an unexpected IO Error.
     */
    public TrimetAPIResponse<TripPlannerResponse> tripPlanner(TripPlannerRequest request) {
        Call<TripPlannerResponse> tripPlannerCall = trimetClientXml.tripPlanner(appId, request.getFromCoord(),
                request.getFromPlace(), request.getToPlace(), request.getToCoord(), request.getDate(),
                request.getTime(), nullsafeToString(request.getMin()), nullsafeToString(request.getArr()),
                request.getWalk(), nullsafeToString(request.getMode()), nullsafeToString(request.getFormat()),
                String.valueOf(request.getMaxItineraries()));
        try {
            retrofit2.Response<TripPlannerResponse> retrofitResponse = tripPlannerCall.execute();
            if (!retrofitResponse.isSuccessful()) {
                return new TrimetAPIResponse<>(null,
                        // TODO fix possible npe
                        retrofitResponse.errorBody().string(),
                        retrofitResponse.code());
            } else {
                return new TrimetAPIResponse<>(retrofitResponse.body(), retrofitResponse.message(),
                        retrofitResponse.code());
            }
        } catch (IOException e) {
            throw new Trimet4JException(e);
        }
    }


    /**
     * Make a request to the Trimet Detours API
     * @param request Detours API Request
     * @return DetoursResponse response wrapped inside a {@link TrimetAPIResponse}; the {@code TrimetAPIResponse}
     *      provides HTTP status, message, and body -- which will be a {@link DetoursResponse} when successful, or
     *      {@code null} if the request failed.
     * @throws Trimet4JException when there is an unexpected IO Error.
     */
    public TrimetAPIResponse<DetoursResponse> detours(DetoursRequest request) {
        String routes = null;
        if(request.getRouteIds() != null && !request.getRouteIds().isEmpty()) {
            routes = String.join(",", request.getRouteIds());
        }
        Call<DetoursResultSet> detoursCall = trimetClientJson.detours(appId, routes, request.isIncludeFuture());
        try {
            Response<DetoursResultSet> detoursExecute = detoursCall.execute();
            if (!detoursExecute.isSuccessful()) {
                return new TrimetAPIResponse<>(null,
                        detoursExecute.errorBody().string(),// TODO fix possible npe
                        detoursExecute.code());
            } else {
                // TODO fix possible npe
                return new TrimetAPIResponse<>(detoursExecute.body().getResultSet(), detoursExecute.message(),
                        detoursExecute.code());
            }
        } catch (IOException e) {
            throw new Trimet4JException(e);
        }
    }






    private static String nullsafeToString(Object o) {
        return (o == null) ? null : o.toString();
    }



























    public static void main(String[] args) throws IOException {

        Properties props = new Properties();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://developer.trimet.org")
//                .addConverterFactory(SimpleXmlConverterFactory.createNonStrict())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        TrimetRetrofitClient client = retrofit.create(TrimetRetrofitClient.class);
        Properties properties = new Properties();
        String appId = null;
        InputStream propertiesFileStream = Trimet4J.class.getResourceAsStream("/com/caryyoung/trimet4j/trimet4j.properties");
        InputStream propertiesLocalFileStream = Trimet4J.class.getResourceAsStream("/com/caryyoung/trimet4j/trimet4j-local.properties");

        try {
            properties.load(propertiesFileStream);
            properties.load(propertiesLocalFileStream);
            appId = properties.getProperty("TRIMET_APP_ID");
        } catch (IOException e) {
            e.printStackTrace(); //TODO something more robust
        }

//        Call<TripPlannerResponse> s = xmlRetrofit.tripPlanner(appId, null, "pdx", "zoo", null
//                , null, "11114:45 pm", null, null, null, null, null, "7");


        Call<StopLocationResultSet> stopLocationResponseCall = client.stopLocation(appId, null, "45.518707,-122.683454", 2000, null);
        try {
            retrofit2.Response<StopLocationResultSet> execute = stopLocationResponseCall.execute();
            System.out.println(execute);
            StopLocationResultSet body = execute.body();
            System.out.println();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
