package com.caryyoung.trimet4j.model.stoplocation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Information about a particular transit route that serves a particular {@link StopLocation}.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StopLocationRoute {

    /**
     * Trimet Route ID (e.g. this will be "20" for the Burnside/Stark bus).
     */
    private String route;

    /**
     * String indicating which type of transit serves this route.  Will be 'B' for bus and 'R' for rail, with no
     * disambiguation between streetcar and MAX.
     */
    private String type;


    private List<StopLocationRouteDirection> dir;

    /**
     * Human-readable description of this route's name and ID, e.g. '20-Burnside/Stark'
     */
    private String desc;
}
