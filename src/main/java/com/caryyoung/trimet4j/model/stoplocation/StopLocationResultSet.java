package com.caryyoung.trimet4j.model.stoplocation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * root node/wrapper that the Stop Location API puts around the results of a request.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StopLocationResultSet {
    private StopLocationResponse resultSet;
    private String errorMessage;
}
