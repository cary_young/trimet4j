package com.caryyoung.trimet4j.model.stoplocation;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by cary on 3/13/18.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StopLocationRequest {


    private Double boundaryBoxLowerLeftLatitude;
    private Double boundaryBoxLowerLeftLongitude;
    private Double boundaryBoxUpperRightLatitude;
    private Double boundaryBoxUpperRightLongitude;

    private Double searchRadiusCenterLatitude;
    private Double searchRadiusCenterLongitude;
    private Integer searchRadiusFeet;
    private Integer searchRadiusMeters;

    /**
     * Construct a {@link StopLocationRequest} for all Stop Locations within a "boundary box" specified by providing
     * the latitude/longitude coordinates of the lower-left corner of the box and the upper-right corner of the box.
     * For example, the boundary box encompassing Ladd's Addition would (roughly) have lower-left coordinates of
     * 45.504902,-122.653748 and upper-right coordinates of 45.512149,-122.645079
     */
    public static StopLocationRequest fromBoundaryBox(Double lowerLeftLat,
                                                      Double lowerLeftLong,
                                                      Double upperRightLat,
                                                      Double upperRightLong) {
        return new StopLocationRequest(lowerLeftLat, lowerLeftLong, upperRightLat, upperRightLong,
                null, null, null, null);
    }

    /**
     * Construct a {@link StopLocationRequest} for all Stop Locations within a specified distance from a central
     * location.
     * @param centerLat latitude of the center point
     * @param centerLong longitude of the center point
     * @param searchRadiusInFeet search radius distance in feet from the central point
     */
    public static StopLocationRequest fromSearchRadiusFeet(Double centerLat,
                                                           Double centerLong,
                                                           Integer searchRadiusInFeet) {
        return new StopLocationRequest(null, null, null,
                null, centerLat, centerLong, searchRadiusInFeet, null);
    }

    /**
     * Construct a {@link StopLocationRequest} for all Stop Locations within a specified distance from a central
     * location.
     * @param centerLat latitude of the center point
     * @param centerLong longitude of the center point
     * @param searchRadiusMeters search radius distance in meters from the central point
     */
    public static StopLocationRequest fromSearchRadiusMeters(Double centerLat,
                                                             Double centerLong,
                                                             Integer searchRadiusMeters) {
        return new StopLocationRequest(null, null, null,
                null, centerLat, centerLong, null, searchRadiusMeters);


    }
}
