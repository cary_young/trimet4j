package com.caryyoung.trimet4j.model.stoplocation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by cary on 3/13/18.
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class StopLocation {

    /**
     * Information about transit routes that stop at this Stop.
     */
    private List<StopLocationRoute> route;

    /**
     * Longitude of the location of this Stop.
     */
    private Double lng;

    /**
     * Cardinal direction of the traffic for this Stop. Will be 'Westbound', 'Eastbound', 'Southbound', or 'Northbound'.
     * For bidirectional rail platforms the value will be blank.
     */
    private String dir;

    /**
     * Latitude of the location of this Stop.
     */
    private Double lat;

    /**
     * Trimet Stop Location ID for this Stop.
     */
    private String locId;

    /**
     * Human-readable description of the location of this stop (e.g. cross-streets)
     */
    private String desc;
}
