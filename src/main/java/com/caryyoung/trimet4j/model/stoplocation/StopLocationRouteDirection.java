package com.caryyoung.trimet4j.model.stoplocation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Information about the direction a particular transit route is headed.
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class StopLocationRouteDirection {

    /**
     * This will be either 1 or 0, where 1 means 'inbound' and '0' means 'outbound'.
     */
    private Integer dir;

    /**
     * Human-readable description of this route direction, e.g. "To South Waterfront"
     */
    private String desc;
}
