package com.caryyoung.trimet4j.model.stoplocation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Response of a request to the Trimet Stop Location API.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StopLocationResponse {

    /**
     * timestamp of when the request was made, formatted: "yyyy-MM-ddTHH:mm:ss.SSSZ"
     */
    private String queryTime;

    /**
     * List of Stop Locations that fall within the boundaries specified in the request.
     */
    private List<StopLocation> location;
}
