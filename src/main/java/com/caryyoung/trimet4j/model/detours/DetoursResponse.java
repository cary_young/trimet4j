package com.caryyoung.trimet4j.model.detours;

import java.util.List;

/**
 * Created by cary on 3/13/18.
 */

public class DetoursResponse {
    /**
     * a list of all detours that match the query from the request.
     */
    private List<Detour> detour;

}

