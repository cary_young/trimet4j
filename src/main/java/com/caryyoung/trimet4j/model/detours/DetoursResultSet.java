package com.caryyoung.trimet4j.model.detours;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * root node/wrapper that the Trimet Detours API puts around the results of a request.
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class DetoursResultSet {
    private DetoursResponse resultSet;
    private String errorMessage;
}
