package com.caryyoung.trimet4j.model.detours;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Information representing a request to the Trimet Detours API.
 */
@NoArgsConstructor
@Data
@AllArgsConstructor
public class DetoursRequest {

    /**
     * (nullable) a list of route IDs that is an optional filter upon the list of detours returned.  If this list
     * is non-null/empty, only detours that affect the specified routes will be returned.
     */
    private List<String> routeIds;

    /**
     * If {@code true}, the list of detours in the response may include detours whose 'begin' date is in the future.
     */
    private boolean includeFuture;
}
