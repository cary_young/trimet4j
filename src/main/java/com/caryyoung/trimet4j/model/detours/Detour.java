package com.caryyoung.trimet4j.model.detours;

import java.util.List;

/**
 * Created by cary on 3/13/18.
 */
public class Detour {
    /**
     * A phonetic spelling of the route detour. This field is used by TriMet's 238-Ride text-to-speech system
     */
    private String phonetic;

    private List<DetourRoute> route;

    /**
     * The time the detour will become invalid. Note that this will always be a time after the time the query was made.
     * Some end times will be very far in the future and will be removed once the detour is no longer in effect.
     *
     * Format: 'yyyy-MM-ddTHH:mm:ss.SSSZ'
     */
    private String end;

    /**
     * A unique identifier of the detour.
     */
    private String id;

    /**
     * Time the detour begins. This will always be a time prior to the time the query was made.
     *
     * Format: 'yyyy-MM-ddTHH:mm:ss.SSSZ'
     */
    private String begin;

    /**
     * A plain text description of the detour.
     */
    private String desc;
}
