package com.caryyoung.trimet4j.model.detours;

/**
 * Created by cary on 3/13/18.
 */
public class DetourRoute {

    /**
     * This route's route ID.
     */
    String route;

    /**
     * Undocumented field; From experience, I think this is always {@link true}.
     */
    boolean detour;

    /**
     * The type of the route, either 'B' for bus, or 'R' for fixed guidway (either rail or aerial tram).
     */
    String type;

    /**
     * Human-readable description of this route. e.g. "MAX Blue Line"
     */
    String desc;
}
