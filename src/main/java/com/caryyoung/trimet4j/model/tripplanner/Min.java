package com.caryyoung.trimet4j.model.tripplanner;

/**
 * Enum representing the allowable values for the {@code min} parameter of the Trip Planner API.  This parameter
 * describes the desired prioritization of time, number of transfers, and walking distance
 */
public enum Min {
    /**
     * prioritize a quick trip.
     */
    T,

    /**
     * minimize the number of transfers in the itinerary.
     */
    X,

    /**
     * minimize the amount of walking in the itinerary.
     */
    W
}
