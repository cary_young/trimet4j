package com.caryyoung.trimet4j.model.tripplanner;

/**
 * Enum representing the allowable values for the {@code arr} parameter of the Trip Planner API.
 */
public enum Arr {

    /**
     * signifies that the time specified in the query represents the time after which the caller wants to depart.
     */
    D,

    /**
     * signifies that the time specified in the query represents the time by which the caller wants to arrive
     * at their destination.
     */
    A
}
