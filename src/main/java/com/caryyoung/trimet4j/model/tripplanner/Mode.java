package com.caryyoung.trimet4j.model.tripplanner;

/**
 * Enum representing the allowable values for the {@code mode} parameter of the Trip Planner API.  This parameter
 * describes the allowable modes of transportation in the itinerary.
 */
public enum Mode {

    /**
     * All modes of transportation are allowed.
     */
    A,

    /**
     * only bus travel is allowed.
     */
    B,

    /**
     * only train travel is allowed.
     */
    T
}
