package com.caryyoung.trimet4j.model.tripplanner;

/**
 * Enum representing the allowable values for the {@code format} parameter of the Trip Planner API.  The
 * only allowable value is "XML".
 */
public enum Format {
    XML
}
