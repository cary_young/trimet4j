package com.caryyoung.trimet4j.model.tripplanner;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * Buildable wrapper describing a request to the Trip Planner API.
 *
 * For official API documentation, see https://developer.trimet.org/ws_docs/tripplanner_ws.shtml
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TripPlannerRequest {

    private String appId;

    private String fromCoord;

    private String fromPlace;

    private String toPlace;

    private String toCoord;

    private String date;

    private String time;

    private Min min;

    private Arr arr;

    private Float walk;

    private Mode mode;

    private Format format;

    private Integer maxItineraries;

}