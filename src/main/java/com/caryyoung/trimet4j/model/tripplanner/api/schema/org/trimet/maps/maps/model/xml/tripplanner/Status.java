//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.06.01 at 10:31:16 PM PDT 
//


package com.caryyoung.trimet4j.model.tripplanner.api.schema.org.trimet.maps.maps.model.xml.tripplanner;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Status.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Status">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token">
 *     &lt;enumeration value="Detour"/>
 *     &lt;enumeration value="Route Status"/>
 *     &lt;enumeration value="Loss of Service"/>
 *     &lt;enumeration value="Announcement"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Status")
@XmlEnum
public enum Status {

    @XmlEnumValue("Detour")
    DETOUR("Detour"),
    @XmlEnumValue("Route Status")
    ROUTE_STATUS("Route Status"),
    @XmlEnumValue("Loss of Service")
    LOSS_OF_SERVICE("Loss of Service"),
    @XmlEnumValue("Announcement")
    ANNOUNCEMENT("Announcement");
    private final String value;

    Status(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Status fromValue(String v) {
        for (Status c: Status.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
